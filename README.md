# uTest

uTest is a unit test for the libft based on the [MinUnit] (http://www.jera.com/techinfo/jtns/jtn002.html) testing framework

![uTest](img/uTest.png)

## How does it work ?

First you need to edit the makefile by changing the variable LIBFT_PATH by the path of your libft
```makefile
NAME = uTest

LIBFT_PATH = #your libft path#

#...#
```

And then you can run ```make``` in your terminal.

You can run the uTest by running ```make run``` ```./uTest```

## How to add other tests ?

Create a new c file in the tests folder.

All of the test files must be written the way
```c
//tests/name_of_your_test.c
#include "minunit.h"

char    *name_of_your_test()
{
    /*
        ...
    */
    mu_assert("Short text if this test fails", test);
    /*
        Exemple:
            mu_assert("strlen("hello") != 5", strlen("hello") == 5);
            If strlen("hello") is not equal to 5, then the test fails and strlen("hello") != 5 will be displayed
            
        You can add as many mu_assert as you want, but be aware that if one test fails, name_of_your_test will return the error message and further tests won't be tested
    */
    return (0); //If all the tests succeed name_of_your_test will return 0
}
```

Then you need to include your new test in include/tests.h

```c
//include/tests.h

#ifndef TESTS_H
# define TESTS_H

/*
    ...
*/
char    *name_of_your_test();

#endif
```

... And add it to the lists of tests in all_tests function in main.c
```c
//main.c

static	void	all_tests()
{
    /*
        ...
    */
    mu_run_test(name_of_your_test);
}
```
---

```Warning : uTest is my own interpretation of what the libft's functions must do.
Some case are deliberately not tested because I don't think that they should be ever used that way (I.E ft_strlen(NULL) and many others).
If I have forgotten to test a case please notice me by either submitting an issue or a merge requests :)```