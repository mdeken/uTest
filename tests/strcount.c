/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcount.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 16:37:28 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/10 16:41:41 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strcount()
{
	mu_assert("ft_strcount(\"\", a) != 0", ft_strcount("", 'a') == 0);
	mu_assert("ft_strcount(\"a\", a) != 1", ft_strcount("a", 'a') == 1);
	mu_assert("ft_strcount(\"aaaa\", a) != 4", ft_strcount("aaaa", 'a') == 4);
	mu_assert("ft_strcount(\"hello\", c) != 0", ft_strcount("hello", 'c') == 0);
	return (0);
}
