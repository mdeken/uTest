/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   striter.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:57:07 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 19:01:10 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

int	test;

static void	count(char *str)
{
	test++;
	(void)str;
}

char	*test_striter()
{
	char	test1[] = "coucou";

	test = 0;
	ft_striter(test1, count);
	mu_assert("ft_striter(test1) != 6", test == 6);
	ft_striter("", count);
	mu_assert("ft_striter(\"\") != 0", test == 6);
	return (0);
}
