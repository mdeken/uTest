#include "minunit.h"
#include <math.h>

#define	EPSILON	0.000001

static int	test_double(double a, double b)
{
	return (fabs(a - b) <= EPSILON);
}

char	*test_sqrt()
{
	mu_assert("ft_sqrt(0) != 0", ft_sqrt(0) == (double)0);
	mu_assert("ft_sqrt(0.0) != 0", ft_sqrt(0.0) == (double) 0);
	mu_assert("ft_sqrt(4) != 2", ft_sqrt(4) == 2);
	mu_assert("ft_sqrt(25) != 5", ft_sqrt(25) == 5);
	mu_assert("ft_sqrt(2) != 1.414214 (+- 0.000001)", test_double(ft_sqrt(2), sqrt(2)));
	mu_assert("ft_sqrt(0.123456789) != 0.351364 (+- 0.000001)", test_double(ft_sqrt(0.123456789), sqrt(0.123456789)));
	mu_assert("ft_sqrt(123.123) != 11.096080 (+- 0.000001)", test_double(ft_sqrt(123.123), sqrt(123.123)));
	mu_assert("ft_sqrt(0.1) != 0.316228 (+- 0.000001)", test_double(ft_sqrt(0.1), sqrt(0.1)));
	return (0);
}
