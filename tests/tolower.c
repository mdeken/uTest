/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tolower.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 17:41:51 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 17:50:41 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_tolower()
{
	mu_assert("ft_tolower('a') != 'a'", ft_tolower('a') == 'a');
	mu_assert("ft_tolower('z') != 'z'", ft_tolower('z') == 'z');
	mu_assert("ft_tolower('A') != 'a'", ft_tolower('A') == 'a');
	mu_assert("ft_tolower('1') != '1'", ft_tolower('1') == '1');
	mu_assert("ft_tolower(' ') != ' '", ft_tolower(' ') == ' ');
	mu_assert("ft_tolower('Z') != 'z'", ft_tolower('Z') == 'z');
	mu_assert("ft_tolower(78547) != 78547", ft_tolower(78547) == 78547);
	mu_assert("ft_tolower('d') != 'd'", ft_tolower('d') == 'd');
	mu_assert("ft_tolower('D') != 'd'", ft_tolower('D') == 'd');
	mu_assert("ft_tolower(-1) != -1", ft_tolower(-1) == -1);
	mu_assert("ft_tolower(0) != 0", ft_tolower(0) == 0);
	return (0);
}
