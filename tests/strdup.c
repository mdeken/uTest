/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:39:34 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:47:58 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <stdlib.h>

char	*test_strdup()
{
	char	*test;
	char	str[] = "Lorem ipsum";

	test = ft_strdup("hello");
	mu_assert("ft_strdup(\"hello\") != hello", strcmp(test, "hello") == 0);
	free(test);
	test = ft_strdup(NULL);
	mu_assert("ft_strdup(NULL) != NULL", test == NULL);
	test = ft_strdup(str);
	mu_assert("ft_strdup(\"Lorem ipsum\") != Lorem ipsum", strcmp("Lorem ipsum", test) == 0);
	mu_assert("ft_strdup(test) == test", test != str);
	free(test);
	test = ft_strdup("");
	mu_assert("ft_strdup(\"\") != \"\"", strcmp(test, "") == 0);
	free(test);
	return (0);
}
