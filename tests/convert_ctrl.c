/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_ctrl.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:42:22 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/10 15:54:52 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <stdlib.h>

char	*test_convert_ctrl()
{
	char	*test;

	test = ft_convert_ctrl("a");
	mu_assert("ft_convert_ctrl(\"a\") != \"a\"", strcmp(test, "a") == 0);
	free(test);

	test = ft_convert_ctrl("");
	mu_assert("ft_convert_ctrl(\"\") != \"\"", strcmp(test, "") == 0);
	free(test);

	test = ft_convert_ctrl("hello\\nWorld");
	mu_assert("ft_convert_ctrl(\"hello\\\\nWorld\") != \"hello\\nWorld\"", strcmp(test, "hello\nWorld") == 0);
	free(test);

	test = ft_convert_ctrl("hello\\cWorld");
	mu_assert("ft_convert_ctrl(\"hello\\cWorld\") != \"hello\"", strcmp(test, "hello") == 0);
	free(test);

	test = ft_convert_ctrl("\\t\\n");
	mu_assert("ft_convert_ctrl(\"\\t\\n\") != \"\\t\\n\"", strcmp(test, "\t\n") == 0);
	free(test);

	test = ft_convert_ctrl("\\t");
	mu_assert("ft_convert_ctrl(\"\\t\") != \"\\t\"", strcmp(test, "\t") == 0);
	free(test);
	return (0);
}
