/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstlen.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 17:44:32 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:03:09 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include "libft.h"
#include <stdlib.h>

static void	free_lst(void *content, size_t size)
{
	char	*str;

	(void)size;
	str = (char *)content;
	free(str);
	str = NULL;
}

char	*test_lstlen()
{
	t_list	*node;
	t_list	*lst;

	node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, node);
	mu_assert("ft_lstlen() != 1", ft_lstlen(lst) == 1);
	node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, node);
	mu_assert("ft_lstlen() != 2", ft_lstlen(lst) == 2);
	node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, node);
	mu_assert("ft_lstlen() != 3", ft_lstlen(lst) == 3);
	node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, node);
	mu_assert("ft_lstlen() != 4", ft_lstlen(lst) == 4);
	ft_lstdel(&lst, free_lst);
	lst = NULL;
	mu_assert("ft_lstlen() != 0", ft_lstlen(lst) == 0);
	return (0);
}
