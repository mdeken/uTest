#include "minunit.h"
#include <stdlib.h>

static void	free_node(void *content, size_t size)
{
	(void) size;
	free(content);
	size = 0;
	content = NULL;
}

char	*test_lstdel()
{
	t_list	*lst;
	t_list	*new_node;

	lst = NULL;
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstadd(&lst, new_node);
	ft_lstdel(&lst, free_node);
	mu_assert("List not deleted (lst != NULL)", lst == NULL);
	return (0);
}
