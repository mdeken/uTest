/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:56:02 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 19:05:14 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strcmp()
{
	mu_assert("ft_strcmp(\"a\", \"aa\") >= 0", ft_strcmp("a", "aa") < 0);
	mu_assert("ft_strcmp(\"aa\", \"a\") <= 0", ft_strcmp("aa", "a") > 0);
	mu_assert("ft_strcmp(\"\", \"\") != 0", ft_strcmp("", "") == 0);
	mu_assert("ft_strcmp(\"abc\", \"\") <= 0", ft_strcmp("abc", "") > 0);
	mu_assert("ft_strcmp(\"\", \"abc\") >= 0", ft_strcmp("", "abc") < 0);
	mu_assert("ft_strcmp(\"hello\", \"heppo\") >= 0", ft_strcmp("hello", "heppo") < 0);
	mu_assert("ft_strcmp(\"heppo\", \"hello\") <= 0", ft_strcmp("heppo", "hello") > 0);
	return (0);
}
