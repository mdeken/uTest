/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putstrl.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:17:07 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:17:16 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output()
{
	int		value;
	int		p[2];
	int		out;
	char	buf[1000];
	char	*output;

	out = dup(1);
	pipe(p);
	dup2(p[1], 1);
	ft_putstrl("hello", 4);
	ft_putstrl("a", 3);
	ft_putstrl("", 4);
	ft_putstrl(NULL, 0);
	ft_putstrl("lorem ipsum", 13);
	dup2(out, 1);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	close(out);
	output = strdup(buf);
	return (output);
}

char	*test_putstrl()
{
	char	*output;
	output = get_output();
	mu_assert("ft_putstrl wrong output", strcmp(output, "helloa      (null)lorem ipsum  ") == 0);
	free(output);
	return (0);
}
