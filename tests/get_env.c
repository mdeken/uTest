/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_env.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 14:29:56 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/14 14:47:42 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

#include <stdlib.h>

extern char **environ;

char	*test_get_env()
{
	char	*value;
	char	*value_test;

	value = NULL;
	value = ft_get_env("TOTO", environ);
	mu_assert("ft_get_env(\"TOTO\") != NULL", value == NULL);
	value = ft_get_env("USER", environ);
	value_test = getenv("USER");
	mu_assert("ft_get_env(\"USER\") != $USER", strcmp(value, value_test) == 0);
	free(value);
	return (0);
}
