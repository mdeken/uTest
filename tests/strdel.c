/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:45:39 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:50:44 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strdel()
{
	char	*test;

	test = strdup("coucou");
	ft_strdel(&test);
	mu_assert("Error ft_strdel", test == NULL);
	return (0);
}
