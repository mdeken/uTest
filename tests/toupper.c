/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   toupper.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 17:41:51 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 17:47:46 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_toupper()
{
	mu_assert("ft_toupper('a') != 'A'", ft_toupper('a') == 'A');
	mu_assert("ft_toupper('z') != 'Z'", ft_toupper('z') == 'Z');
	mu_assert("ft_toupper('A') != 'A'", ft_toupper('A') == 'A');
	mu_assert("ft_toupper('1') != '1'", ft_toupper('1') == '1');
	mu_assert("ft_toupper(' ') != ' '", ft_toupper(' ') == ' ');
	mu_assert("ft_toupper('Z') != 'Z'", ft_toupper('Z') == 'Z');
	mu_assert("ft_toupper(78547) != 78547", ft_toupper(78547) == 78547);
	mu_assert("ft_toupper('d') != 'D'", ft_toupper('d') == 'D');
	mu_assert("ft_toupper(-1) != -1", ft_toupper(-1) == -1);
	mu_assert("ft_toupper(0) != 0", ft_toupper(0) == 0);
	return (0);
}
