/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strbefore.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 13:34:12 by mdeken            #+#    #+#             */
/*   Updated: 2016/12/08 13:43:16 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include "libft.h"
#include <stdlib.h>
#include <stdio.h>

char	*test_strbefore()
{
	char	*test;

	test = ft_strbefore("tototata", 'a');
	mu_assert("ft_strbefore(\"tototata\", 'a') != \"totot\"", strcmp(test, "totot") == 0);
	free(test);
	test = ft_strbefore("toto", 't');
	mu_assert("ft_strbefore(\"toto\", 't') != \"\"", strcmp(test, "") == 0);
	free(test);
	test = ft_strbefore("toto", 'i');
	mu_assert("ft_strbefore(\"toto\", 'i') != NULL", test == NULL);
	test = ft_strbefore("hello", 'o');
	mu_assert("ft_strbefore(\"hello\", 'o') != \"hell\"", strcmp(test, "hell") == 0);
	free(test);
	test = ft_strbefore("hello", '\0');
	mu_assert("ft_strbefore(\"hello\", '\\0') != \"hello\"", strcmp(test, "hello") == 0);
	free(test);
	test = ft_strbefore("", 'o');
	mu_assert("ft_strbefore(\"\", 'o') != NULL", test == NULL);
	return (0);
}
