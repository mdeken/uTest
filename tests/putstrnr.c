/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putstrnr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 19:20:50 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 19:21:47 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output()
{
	int		value;
	int		p[2];
	int		out;
	char	buf[1000];
	char	*output;

	out = dup(1);
	pipe(p);
	dup2(p[1], 1);
	ft_putstrnr("hello", 3, 5);
	ft_putstrnr("a", 1, 3);
	ft_putstrnr("", 0, 4);
	ft_putstrnr("lorem ipsum",5 ,7);
	ft_putstrnr("lorem ipsum",0 ,3);
	dup2(out, 1);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	close(out);
	output = strdup(buf);
	return (output);
}

char	*test_putstrnr()
{
	char	*output;
	output = get_output();
	mu_assert("ft_putstrnr wrong output", strcmp(output, "  hel  a      lorem   ") == 0);
	free(output);
	return (0);
}
