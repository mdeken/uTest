/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putnbr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:28:00 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:37:36 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output()
{
	int		value;
	int		p[2];
	int		out;
	char	buf[1000];
	char	*output;

	out = dup(1);
	pipe(p);
	dup2(p[1], 1);
	ft_putnbr(10);
	ft_putnbr(9);
	ft_putnbr(11);
	ft_putnbr(0);
	ft_putnbr(1);
	ft_putnbr(-1);
	ft_putnbr(-10);
	ft_putnbr(-9);
	ft_putnbr(-11);
	ft_putnbr(-2147483648);
	ft_putnbr(2147483647);
	ft_putnbr(123);
	ft_putnbr(-123);
	dup2(out, 1);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	close(out);
	output = strdup(buf);
	return (output);
}

char	*test_putnbr()
{
	char	*output;
	output = get_output();
	mu_assert("ft_putnbr wrong output", strcmp(output, "1091101-1-10-9-11-21474836482147483647123-123") == 0);
	free(output);
	return (0);
}
