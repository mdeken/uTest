/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strjoinc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 14:16:24 by mdeken            #+#    #+#             */
/*   Updated: 2016/12/09 14:26:32 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include "libft.h"
#include <stdlib.h>

char	*test_strjoinc()
{
	char	*test;

	test = ft_strjoinc("", "", 'c');
	mu_assert("ft_strjoinc(\"\", \"\", 'c') != \"c\"", strcmp(test, "c") == 0);
	free(test);
	test = ft_strjoinc("1+1", "2", '=');
	mu_assert("ft_strjoinc(\"1+1\", \"2\", '=') != \"1+1=2\"", strcmp(test, "1+1=2") == 0);
	free(test);
	test = ft_strjoinc("", "b", 'a');
	mu_assert("ft_strjoinc(\"\", \"b\", 'a') != \"ab\"", strcmp(test, "ab") == 0);
	free(test);
	test = ft_strjoinc("a", "", 'b');
	mu_assert("ft_strjoinc(\"a\", \"\", 'b') != \"ab\"", strcmp(test, "ab") == 0);
	free(test);
	return (0);
}
