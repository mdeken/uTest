/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 11:38:47 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/03 12:17:50 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <stdlib.h>

static int	cmp_str(t_list *lst1, t_list *lst2)
{
	char	*str1;
	char	*str2;

	str1 = lst1->content;
	str2 = lst2->content;
	return (strcmp(str1, str2));
}


static void	del_str(void *content, size_t size)
{
	free(content);
	content = NULL;
	(void) size;
}

static int	test(t_list *lst1, t_list *lst2)
{
	int	ret_value;
//	char	*test;

	ret_value = ft_lstcmp(lst1, lst2, cmp_str);
	ft_lstdel(&lst2, del_str);
	ft_lstdel(&lst1, del_str);
	return (ret_value);
}

char	*test_lstcmp()
{
	t_list	*lst1;
	t_list	*lst2;
	t_list	*new_node;

	lst1 = NULL;
	lst2 = NULL;
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("c", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("c", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("d", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("d", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("e", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("e", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	mu_assert("ft_lstcmp != 0", test(lst1, lst2) == 0);

	lst1 = NULL;
	lst2 = NULL;
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	mu_assert("ft_lstcmp !< 0", test(lst1, lst2) < 0);

	lst1 = NULL;
	lst2 = NULL;
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	mu_assert("ft_lstcmp !> 0", test(lst1, lst2) > 0);

	lst1 = NULL;
	lst2 = NULL;
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	mu_assert("ft_lstcmp !< 0", test(lst1, lst2) < 0);

	lst1 = NULL;
	lst2 = NULL;
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst2, new_node);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddback(&lst1, new_node);
	mu_assert("ft_lstcmp !> 0", test(lst1, lst2) > 0);

	lst1 = NULL;
	lst2 = NULL;
	mu_assert("ft_lstcmp != 0", test(lst1, lst2) == 0);
	return (0);	
}
