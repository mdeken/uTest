/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isalnum.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:36:41 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 16:41:53 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_isalnum()
{
	mu_assert("ft_isalnum('a') == 0", ft_isalnum('a') > 0);
	mu_assert("ft_isalnum('%') != 0", ft_isalnum('%') == 0);
	mu_assert("ft_isalnum(']') != 0", ft_isalnum(']') == 0);
	mu_assert("ft_isalnum(-1) != 0", ft_isalnum(-1) == 0);
	mu_assert("ft_isalnum(123) != 0", ft_isalnum(123) == 0);
	mu_assert("ft_isalnum(1999523) != 0", ft_isalnum(1999523) == 0);
	mu_assert("ft_isalnum(0) != 0", ft_isalnum(0) == 0);
	mu_assert("ft_isalnum('Z') == 0", ft_isalnum('Z') > 0);
	mu_assert("ft_isalnum('4') == 0", ft_isalnum('4') > 0);
	mu_assert("ft_isalnum('0') == 0", ft_isalnum('0') > 0);
	mu_assert("ft_isalnum('9') == 0", ft_isalnum('9') > 0);
	mu_assert("ft_isalnum('z') == 0", ft_isalnum('z') > 0);
	return (0);
}
