/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isprint.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:36:41 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 16:54:24 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_isprint()
{
	mu_assert("ft_isprint('a') == 0", ft_isprint('a') > 0);
	mu_assert("ft_isprint('%') == 0", ft_isprint('%') > 0);
	mu_assert("ft_isprint(']') == 0", ft_isprint(']') > 0);
	mu_assert("ft_isprint(-1) != 0", ft_isprint(-1) == 0);
	mu_assert("ft_isprint(123) == 0", ft_isprint(123) > 0);
	mu_assert("ft_isprint(1999523) != 0", ft_isprint(1999523) == 0);
	mu_assert("ft_isprint(0) != 0", ft_isprint(0) == 0);
	mu_assert("ft_isprint('Z') == 0", ft_isprint('Z') > 0);
	mu_assert("ft_isprint('4') == 0", ft_isprint('4') > 0);
	mu_assert("ft_isprint('0') == 0", ft_isprint('0') > 0);
	mu_assert("ft_isprint('9') == 0", ft_isprint('9') > 0);
	mu_assert("ft_isprint('z') == 0", ft_isprint('z') > 0);
	mu_assert("ft_isprint('\n') != 0", ft_isprint('\n') == 0);
	mu_assert("ft_isprint(6) != 0", ft_isprint(6) == 0);
	return (0);
}
