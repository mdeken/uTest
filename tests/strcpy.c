/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcpy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:40:26 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:44:31 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strcpy()
{
	char	test1[] = "coucou";
	char	test2[7];

	ft_strcpy(test2, test1);
	mu_assert("Error1 ft_strcpy", strcmp(test1, test2) == 0);
	ft_strcpy(test2, "");
	mu_assert("Error2 ft_strcpy", strcmp("", test2) == 0);
	return (0);
}
