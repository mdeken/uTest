/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 15:48:49 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/23 16:19:54 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <stdlib.h>

char	*test_itoa()
{
	char	*test;

	test = NULL;
	test = ft_itoa(123);
	mu_assert("ft_itoa(123) == NULL", test != NULL);
	mu_assert("ft_itoa(123) != \"123\"", strcmp(test, "123") == 0);
	free(test);
	test = NULL;
	test = ft_itoa(-123);
	mu_assert("ft_itoa(-123) == NULL", test != NULL);
	mu_assert("ft_itoa(-123) != \"123\"", strcmp(test, "-123") == 0);
	mu_assert("ft_itoa(-123) -> missing \\0", test[4] == '\0');
	free(test);
	test = NULL;
	test = ft_itoa(0);
	mu_assert("ft_itoa(0) != \"0\"", strcmp(test, "0") == 0);
	free(test);
	test = NULL;
	test = ft_itoa(-2147483648);
	mu_assert("ft_itoa(-2147483648) != \"-2147483648\"", strcmp(test, "-2147483648") == 0);
	free(test);
	test = NULL;
	test = ft_itoa(2147483647);
	mu_assert("ft_itoa(2147483647) != 2147483647", strcmp(test, "2147483647") == 0);
	free(test);
	test = NULL;
   return (0);	
}
