/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 20:25:47 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/23 15:02:56 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_atoi()
{
	mu_assert("ft_atoi(\"0\") != 0", ft_atoi("0") == 0);
	mu_assert("ft_atoi(\"9\") != 9", ft_atoi("9") == 9);
	mu_assert("ft_atoi(\"123\") != 123", ft_atoi("123") == 123);
	mu_assert("ft_atoi(\"aaaaa\") != 0", ft_atoi("aaaaa") == 0);
	mu_assert("ft_atoi(\"123aaa\") != 123", ft_atoi("123aaa") == 123);
	mu_assert("ft_atoi(\"aaa123\") != 0", ft_atoi("aaa123") == 0);
	mu_assert("ft_atoi(\"\") != 0", ft_atoi("") == 0);
	mu_assert("ft_atoi(\"-123\") != -123", ft_atoi("-123") == -123);
	mu_assert("ft_atoi(\"+123\") != 123", ft_atoi("+123") == 123);
	mu_assert("ft_atoi(\"+00123\") != 123", ft_atoi("+00123") == 123);
	mu_assert("ft_atoi(\"--123\") != 0", ft_atoi("--123") == 0);
	mu_assert("ft_atoi(\"   32\") != 32", ft_atoi("   32") == 32);
	mu_assert("ft_atoi(\"-2147483648\") != -2147483648", ft_atoi("-2147483648") == -2147483648);
	mu_assert("ft_atoi(\"2147483647\") != 2147483647", ft_atoi("2147483647") == 2147483647);
	mu_assert("ft_atoi(\"4.6\") != 4", ft_atoi("4.6") == 4);
	mu_assert("ft_atoi(\"+\") != 0", ft_atoi("+") == 0);
	mu_assert("ft_atoi(\"-\") != 0", ft_atoi("-") == 0);
	return (0);
}
