/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:55:20 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:07:44 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <string.h>

char	*test_memset()
{
	char	test0[10], test1[10];
	char	*ptr_test;

	memset(test0, 90, 10);
	ptr_test = ft_memset(test1, 90, 10);
	mu_assert("ft_memset(test1, 90, 10) != test1", ptr_test == test1);
	mu_assert("ft_memset(test1, 90, 10) != memset(test0, 90, 10)", memcmp(test0, test1, 10) == 0);
	memset(test0, 0, 2);
	ft_memset(test1, 0, 2);
	mu_assert("ft_memset(test1, 0, 2) != memset(test0, 0, 2)", memcmp(test0, test1, 2) == 0);
	mu_assert("ft_memset(test1, 0, 2) != memset(test0, 0, 2)", memcmp(test0, test1, 10) == 0);
	ft_memset(test1, 7, 0);
	mu_assert("ft_memset(test1, 7, 0) is doing something", memcmp(test0, test1, 10) == 0);
	return (0);
}
