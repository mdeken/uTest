/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isascii.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:36:41 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 16:45:24 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_isascii()
{
	mu_assert("ft_isascii('a') == 0", ft_isascii('a') > 0);
	mu_assert("ft_isascii('%') == 0", ft_isascii('%') > 0);
	mu_assert("ft_isascii(']') == 0", ft_isascii(']') > 0);
	mu_assert("ft_isascii(-1) != 0", ft_isascii(-1) == 0);
	mu_assert("ft_isascii(123) == 0", ft_isascii(123) > 0);
	mu_assert("ft_isascii(1999523) != 0", ft_isascii(1999523) == 0);
	mu_assert("ft_isascii(0) == 0", ft_isascii(0) > 0);
	mu_assert("ft_isascii('Z') == 0", ft_isascii('Z') > 0);
	mu_assert("ft_isascii('4') == 0", ft_isascii('4') > 0);
	mu_assert("ft_isascii('0') == 0", ft_isascii('0') > 0);
	mu_assert("ft_isascii('9') == 0", ft_isascii('9') > 0);
	mu_assert("ft_isascii('z') == 0", ft_isascii('z') > 0);
	return (0);
}
