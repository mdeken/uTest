/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcchr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:23:53 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:27:53 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strcchr()
{
	mu_assert("ft_strcchr(\"Coucou\", 'c') != 3", ft_strcchr("Coucou", 'c') == 3);
	mu_assert("ft_strcchr(\"coucou\", 'c') != 0", ft_strcchr("coucou", 'c') == 0);
	mu_assert("ft_strcchr(\"coucou\", '\\0') != 6", ft_strcchr("coucou", '\0') == 6);
	mu_assert("ft_strcchr(\"coucou\", 'z') != -1", ft_strcchr("coucou", 'z') == -1);
	mu_assert("ft_strcchr(\"\", 'z') != -1", ft_strcchr("", 'z') == -1);
	return (0);
}
