/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 17:12:03 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 17:40:25 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <string.h>

char 	*test_strcat()
{
	char	test1[20];
	char	test2[] = "hello";
	char	test3[] = "";
	char	test4[] = "oWorld!";
	char	*ptr_test;

	bzero(test1, 20);
	ptr_test = ft_strcat(test2, "");
	mu_assert("Test 1 : ft_strcat(test2, "") != test2", ptr_test == test2);
	mu_assert("Test 2 : test2 != \"hello\"", strcmp(test2, "hello") == 0);

	ptr_test = ft_strcat(test1, test3);
	mu_assert("Test 3 : ft_strcat(test1, test3) != test1", ptr_test == test1);
	mu_assert("Test 4 : test1 != \"\"", strcmp(test1, test3) == 0);

	ptr_test = ft_strcat(test1, test4);
	mu_assert("Test 5 : ft_strcat(test1, test4) != test1", ptr_test == test1);
	mu_assert("Test 6 : test1 != \"oWorld!\"", strcmp(test1, "oWorld!") == 0);

	bzero(test1, 20);
	ft_strcat(test1, test2);
	ft_strcat(test1, test4);
	mu_assert("Test 7 : test1 != \"hellooWorld!\"", strcmp(test1, "hellooWorld!") == 0);
	return (0);
}
