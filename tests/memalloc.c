#include "minunit.h"
#include <stdlib.h>

static int	test_mem(char *mem)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (mem[i] != 0)
			return (0);
		i++;
	}
	return (1);
}

char	*test_memalloc()
{
	char	*test1;

	test1 = NULL;
	test1 = ft_memalloc(4);
	mu_assert("memalloc returns NULL", test1 != NULL);
	mu_assert("memalloc does not initialize to 0", test_mem(test1));
	free(test1);
	test1 = ft_memalloc(0);
	mu_assert("memalloc does not returns NULL when size < 0", test1 == NULL);
	return (0);
}
