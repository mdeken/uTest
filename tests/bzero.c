/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bzero.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:55:20 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/23 14:52:38 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <string.h>

char	*test_bzero()
{
	char	test0[5];
	char	test1[5];
	char	test2[] = "qwertyuiop";
	char	test3[] = "qwertyuiop";
	char	test4[] = "qwertyuiop";
	char	test5[] = "qwertyuiop";

	ft_bzero(test0, 5);
	bzero(test1, 5);
	mu_assert("Test 1 failed : ft_bzero(test0, 5) != bzero(test1, 5)", memcmp(test0, test1, 5) == 0);
	ft_bzero(test2, 2);
	bzero(test3, 2);
	mu_assert("Test 2 failed : ft_bzero(test2, 2) != bzero(test3, 2)", memcmp(test2, test2, 2) == 0);
	mu_assert("Test 3 failed : ft_bzero(test2, 2) != bzero(test3, 2)", memcmp(test2, test2, 11) == 0);
	ft_bzero(test4, 0);
	bzero(test5, 0);
	mu_assert("Test 4 failed : ft_bzero(test4, 0) != bzero(test5, 0)", memcmp(test4, test5, 11) == 0);
	return (0);
}
