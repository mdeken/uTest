#include "minunit.h"
#include <stdlib.h>

char	*test_memdel()
{
	char	*test;

	test = (char *)malloc(2 * sizeof(char));
	ft_memdel((void **)&test);
	mu_assert("test != NULL", test == NULL);
	return (0);
}
