/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bitslen.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 15:05:24 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/23 15:13:40 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_bitslen()
{
	mu_assert("ft_bitslen(0) != 1", ft_bitslen(0) == 1);
	mu_assert("ft_bitslen(10) != 4", ft_bitslen(10) == 4);
	mu_assert("ft_bitslen(1) != 1", ft_bitslen(1) == 1);
	mu_assert("ft_bitslen(2) != 2", ft_bitslen(2) == 2);
	mu_assert("ft_bitslen(51452) != 16", ft_bitslen(51452) == 16);
	return (0);
}
