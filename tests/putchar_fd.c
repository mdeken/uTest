/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putchar.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:28:00 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:37:36 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output(int fd)
{
	int		value;
	int		p[2];
	char	buf[1000];
	char	*output;

	pipe(p);
	dup2(p[1], fd);
	ft_putchar_fd('a', fd);
	ft_putchar_fd('b', fd);
	ft_putchar_fd('\n', fd);
	ft_putchar_fd('1', fd);
	ft_putchar_fd('2', fd);
	ft_putchar_fd('3', fd);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	output = strdup(buf);
	return (output);
}

char	*test_putchar_fd()
{
	char	*output;
	output = get_output(2);
	mu_assert("ft_putchar_fd wrong output", strcmp(output, "ab\n123") == 0);
	free(output);
	return (0);
}
