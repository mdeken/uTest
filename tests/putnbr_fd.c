/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putnbr_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:28:00 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:37:36 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output()
{
	int		value;
	int		p[2];
	char	buf[1000];
	char	*output;

	pipe(p);
	dup2(p[1], 2);
	ft_putnbr_fd(10, 2);
	ft_putnbr_fd(9, 2);
	ft_putnbr_fd(11, 2);
	ft_putnbr_fd(0, 2);
	ft_putnbr_fd(1, 2);
	ft_putnbr_fd(-1, 2);
	ft_putnbr_fd(-10, 2);
	ft_putnbr_fd(-9, 2);
	ft_putnbr_fd(-11, 2);
	ft_putnbr_fd(-2147483648, 2);
	ft_putnbr_fd(2147483647, 2);
	ft_putnbr_fd(123, 2);
	ft_putnbr_fd(-123, 2);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	output = strdup(buf);
	return (output);
}

char	*test_putnbr_fd()
{
	char	*output;
	output = get_output();
	mu_assert("ft_putnbr_fd wrong output", strcmp(output, "1091101-1-10-9-11-21474836482147483647123-123") == 0);
	free(output);
	return (0);
}
