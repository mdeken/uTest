/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putendc_fd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 19:20:50 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 19:21:47 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output(int fd)
{
	int		value;
	int		p[2];
	char	buf[1000];
	char	*output;

	pipe(p);
	dup2(p[1], fd);
	ft_putendc_fd("hello", ' ', fd);
	ft_putendc_fd("a", 'b', fd);
	ft_putendc_fd("", '2', fd);
	ft_putendc_fd(NULL, ' ', fd);
	ft_putendc_fd("lorem ipsum", ' ', fd);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	output = strdup(buf);
	return (output);
}

char	*test_putendc_fd()
{
	char	*output;
	output = get_output(2);
	mu_assert("ft_putendc_fd wrong output", strcmp(output, "hello ab2(null) lorem ipsum ") == 0);
	free(output);
	return (0);
}
