/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   imemset.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 15:21:57 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/23 15:27:48 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

static int	assert_imemset(int *tab, int size, int value)
{
	int	i;

	i = 0;
	while (i < size)
	{
		if (tab[i] != value)
			return 0;
		i++;
	}
	return (1);
}

char	*test_imemset()
{
	int	test1[5];

	ft_imemset(test1, 5, 5);
	mu_assert("ft_imemset(test1, 5, 5) != 5|5|5|5|5",	assert_imemset(test1, 5, 5));
	return (0);
}
