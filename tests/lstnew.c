#include "minunit.h"
#include <stdlib.h>

static void	del(t_list *elem)
{
	if (elem->content != NULL)
		free(elem->content);
	free(elem);
}

char	*test_lstnew()
{
	t_list	*new_node;
	char	*test_content;

	new_node = ft_lstnew("a", sizeof(char *));
	mu_assert("New node == NULL", new_node != NULL);
	test_content = (char *)new_node->content;
	mu_assert("Wrong content", strcmp(test_content, "a") == 0);
	mu_assert("Wrong content size", new_node->content_size == sizeof(char *));
	del(new_node);
	new_node = ft_lstnew(NULL, 10);
	mu_assert("Content not NULL", new_node->content == NULL);
	del(new_node);
	return (0);
}
