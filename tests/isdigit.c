/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isdigit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:13:06 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 16:20:41 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_isdigit()
{
	mu_assert("ft_isdigit('a') != 0", ft_isdigit('a') == 0);
	mu_assert("ft_isdigit(']') != 0", ft_isdigit(']') == 0);
	mu_assert("ft_isdigit('E') != 0", ft_isdigit('E') == 0);
	mu_assert("ft_isdigit(':') != 0", ft_isdigit(':') == 0);
	mu_assert("ft_isdigit('/') != 0", ft_isdigit('/') == 0);
	mu_assert("ft_isdigit('=') != 0", ft_isdigit('=') == 0);
	mu_assert("ft_isdigit(78882) != 0", ft_isdigit(78882) == 0);
	mu_assert("ft_isdigit(-1) != 0", ft_isdigit(-1) == 0);
	mu_assert("ft_isdigit('\\n') != 0", ft_isdigit('\n') == 0);
	mu_assert("ft_isdigit('0') == 0", ft_isdigit('0') > 0);
	mu_assert("ft_isdigit('9') == 0", ft_isdigit('9') > 0);
	mu_assert("ft_isdigit('5') == 0", ft_isdigit('5') > 0);
	return (0);
}
