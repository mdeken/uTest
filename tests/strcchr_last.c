/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcchr_last.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:23:53 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:33:02 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strcchr_last()
{
	mu_assert("ft_strcchr_last(\"Coucou\", 'c') != 3", ft_strcchr_last("Coucou", 'c') == 3);
	mu_assert("ft_strcchr_last(\"coucou\", 'c') != 3", ft_strcchr_last("coucou", 'c') == 3);
	mu_assert("ft_strcchr_last(\"coucou\", '\\0') != 6", ft_strcchr_last("coucou", '\0') == 6);
	mu_assert("ft_strcchr_last(\"coucou\", 'z') != -1", ft_strcchr_last("coucou", 'z') == -1);
	mu_assert("ft_strcchr_last(\"\", 'z') != -1", ft_strcchr_last("", 'z') == -1);
	return (0);
}
