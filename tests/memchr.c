#include "minunit.h"

char	*test_memchr()
{
	char	test1[] = "abc";

	mu_assert("ft_memchr(\"abc\", 'c', 3) != memchr(\"abc\", 'c', 3)", ft_memchr(test1, 'c', 3) == memchr(test1, 'c', 3));
	mu_assert("ft_memchr(\"abc\", 'a', 3) != memchr(\"abc\", 'a', 3)", ft_memchr(test1, 'a', 3) == memchr(test1, 'a', 3));
	mu_assert("ft_memchr(\"abc\", 'c', 2) != memchr(\"abc\", 'c', 2)", ft_memchr(test1, 'c', 2) == memchr(test1, 'c', 2));
	mu_assert("ft_memchr(\"abc\", 'r', 2) != memchr(\"abc\", 'r', 2)", ft_memchr(test1, 'r', 2) == memchr(test1, 'r', 2));
	mu_assert("ft_memchr(\"abc\", 'c', 0) != memchr(\"abc\", 'c', 0)", ft_memchr(test1, 'c', 0) == memchr(test1, 'c', 0));
	return (0);
}
