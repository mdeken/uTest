/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strequ.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:52:53 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:54:58 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strequ()
{
	mu_assert("ft_strequ(\"\", \"test\") != 0", ft_strequ("", "test") == 0);
	mu_assert("ft_strequ(\"test\", \"\") != 0", ft_strequ("test", "") == 0);
	mu_assert("ft_strequ(\"test\", \"test\") != 1", ft_strequ("test", "test") == 1);
	mu_assert("ft_strequ(\"\", \"\") != 1", ft_strequ("", "") == 1);
	return (0);
}
