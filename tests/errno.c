/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errno.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 12:10:04 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/10 13:04:20 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include "ft_errno.h"

char	*test_errno()
{
	t_error *error;

	error = NULL;
	error = ft_geterrno();
	mu_assert("errno not get", error != NULL);
	ft_seterrno((t_error)1);
	mu_assert("errno not set", *error == 1);
	return (0);
}
