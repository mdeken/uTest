#include "minunit.h"
#include <stdlib.h>

static void	free_node(void *content, size_t size)
{
	(void) size;
	free(content);
	size = 0;
	content = NULL;
}

char	*test_lstdelone()
{
	t_list	*lst;
	t_list	*new_node;
	t_list	*new_node1;

	lst = NULL;
	new_node1 = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node1);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstadd(&lst, new_node);
	ft_lstdelone(&lst, free_node);
	mu_assert("Node not deleted properly 1", new_node == NULL);
	mu_assert("Node not deleted properly 2", new_node == lst);
	ft_lstdelone(&lst, free_node);
	mu_assert("Node not deleted properly 3", lst == NULL);
	return (0);
}
