#include "minunit.h"

char	*test_nbrlen()
{
	mu_assert("ft_nbrlen(0) != 1", ft_nbrlen(0) == 1);
	mu_assert("ft_nbrlen(1) != 1", ft_nbrlen(1) == 1);
	mu_assert("ft_nbrlen(9) != 1", ft_nbrlen(9) == 1);
	mu_assert("ft_nbrlen(-1) != 2", ft_nbrlen(-1) == 2);
	mu_assert("ft_nbrlen(10) != 2", ft_nbrlen(10) == 2);
	mu_assert("ft_nbrlen(245) != 3", ft_nbrlen(245) == 3);
	mu_assert("ft_nbrlen(-15) != 3", ft_nbrlen(-15) == 3);
	return (0);
}
