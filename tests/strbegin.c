/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strbegin.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 19:26:09 by mdeken            #+#    #+#             */
/*   Updated: 2016/12/07 19:37:57 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include "libft.h"

char	*test_strbegin()
{
	mu_assert("ft_strbegin(\"toto\", \"tata\") != 0", ft_strbegin("toto", "tata") == 0);
	mu_assert("ft_strbegin(\"tata\", \"tata\") != 1", ft_strbegin("tata", "tata") == 1);
	mu_assert("ft_strbegin(\"toto\", \"toto a la maison\") != 0", ft_strbegin("toto", "toto a la maison") == 0);
	mu_assert("ft_strbegin(\"toto a la maison\", \"toto\") != 1", ft_strbegin("toto a la maison", "toto") == 1);
	mu_assert("ft_strbegin(\"\", \"\") != 1", ft_strbegin("", "") == 1);
	mu_assert("ft_strbegin(\"toto\", \"\") != 1", ft_strbegin("toto", "") == 1);
	mu_assert("ft_strbegin(\"\", \"tata\") != 0", ft_strbegin("", "tata") == 0);
	return (0);
}
