/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putnbrl.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:28:00 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:37:36 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output()
{
	int		value;
	int		p[2];
	int		out;
	char	buf[1000];
	char	*output;

	out = dup(1);
	pipe(p);
	dup2(p[1], 1);
	ft_putnbrl(10, 2);
	ft_putnbrl(9, 3);
	ft_putnbrl(11, 0);
	ft_putnbrl(0, 4);
	ft_putnbrl(-13, 4);
	dup2(out, 1);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	close(out);
	output = strdup(buf);
	return (output);
}

char	*test_putnbrl()
{
	char	*output;
	output = get_output();
	mu_assert("ft_putnbrl wrong output", strcmp(output, "109  110   -13 ") == 0);
	free(output);
	return (0);
}
