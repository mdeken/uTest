/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strclr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/05 18:33:38 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/05 18:39:49 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

static int	test_end(char *str, int size)
{
	int	i = 0;

	while (i < size + 1)
	{
		if (str[i] != '\0')
			return (0);
		i++;
	}
	return (1);
}

char	*test_strclr()
{
	char	test1[] = "coucou";

	ft_strclr(test1);
	mu_assert("ft_strclr(\"coucou\") != \"\\0\\0\\0\\0\\0\\0\\0\"", test_end(test1, 6));
	return (0);
}
