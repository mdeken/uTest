#include "minunit.h"

char	*test_memcmp()
{
	int	test1[] = {1, 2, 3, 4};
	int	test2[] = {-5, 4, 2, 5};
	char	test3[] = "hello";
	char	test4[] = "heppo";

	mu_assert("ft_memcmp(test1, test1, 4 * sizeof(int)) != 0", ft_memcmp(test1, test1, sizeof(int) * 4) == 0);
	mu_assert("ft_memcmp(test1, test2, 4 * sizeof(int)) => 0", ft_memcmp(test1, test2, sizeof(int) * 4) < 0);
	mu_assert("ft_memcmp(test3, test3, 2) != 0", ft_memcmp(test3, test3, 2) == 0);
	mu_assert("ft_memcmp(test3, test4, 2) != 0", ft_memcmp(test3, test4, 2) == 0);
	mu_assert("ft_memcmp(test3, test4, 3) == 0", ft_memcmp(test3, test4, 3) < 0);
	mu_assert("ft_memcmp(test4, test3, 3) == 0", ft_memcmp(test4, test3, 3) > 0);
	return (0);
}
