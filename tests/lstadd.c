/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstadd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 16:22:03 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/23 17:31:11 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <stdlib.h>

static void	free_node(void *content, size_t size)
{
	(void) size;
	if (content != NULL)
		free(content);
	content = NULL;
	size = 0;
}

static int	test_order(t_list *lst)
{
	char	*a;
	char	*b;

	a = (char *)lst->content;
	b = (char *) lst->next->content;
	if (strcmp(a, "2") != 0 || strcmp(b, "1") != 0)
		return (0);
	else
		return (1);
}

char	*test_lstadd()
{
	t_list	*lst;
	t_list	*new_node;

	lst = NULL;
	new_node = NULL;
	ft_lstadd(&lst, new_node);
	mu_assert("ft_lstadd(NULL, NULL) -> != NULL", lst == NULL && new_node == NULL);
	new_node = ft_lstnew("1", sizeof(char *));
	ft_lstadd(&lst, new_node);
	mu_assert("ft_lstadd(&lst, new_node) != new_node | NULL", lst == new_node);
	new_node = ft_lstnew("2", sizeof(char *));
	ft_lstadd(&lst, new_node);
	mu_assert("ft_lstadd(&lst, new_node) != \"2\" | \"1\" | NULL", test_order(lst));
	mu_assert("Missing null at the end", lst->next->next == NULL);
	ft_lstdel(&lst, free_node);
	return (0);
}
