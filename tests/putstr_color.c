/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putstr_color.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 19:20:50 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 19:21:47 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

static char	*get_output()
{
	int		value;
	int		p[2];
	int		out;
	char	buf[1000];
	char	*output;

	out = dup(1);
	pipe(p);
	dup2(p[1], 1);
	ft_putstr_color("hello", "bold", "color", "background");
	ft_putstr_color("a", "bold", "color", NULL);
	ft_putstr_color("", "bold", NULL, NULL);
	ft_putstr_color(NULL, NULL, NULL, NULL);
	dup2(out, 1);
	value = read(p[0], buf, 1000);
	buf[value] = '\0';
	close(p[0]);
	close(p[1]);
	close(out);
	output = strdup(buf);
	return (output);
}

char	*test_putstr_color()
{
	char	*output;
	output = get_output();
	mu_assert("ft_putstr_color wrong output", strcmp(output, "boldcolorbackgroundhello\033[0mboldcolora\033[0mbold\033[0m(null)\033[0m") == 0);
	free(output);
	return (0);
}
