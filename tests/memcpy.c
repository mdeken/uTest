/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memcpy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 18:08:49 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 18:18:26 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_memcpy()
{
	char	test0[20], test1[20];

	memset(test0, 5, 20);
	memset(test1, 8, 20);

	ft_memcpy(test0, test1, 20);
	mu_assert("ft_memcpy(test0, test1, 20) != memcpy(test0, test1, 50)", memcmp(test0, test1, 20) == 0);
	ft_memcpy(test1, test0, 0);
	mu_assert("ft_memcpy(test1, test0, 0) != memcpy(test1, test0, 0)", memcmp(test0, test1, 20) == 0);
	return (0);
}
