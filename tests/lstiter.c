#include "minunit.h"
#include <stdlib.h>

static int		test_iter;

static void	add(t_list *elem)
{
	(void)elem;
	test_iter++;
}

static void	del_str(void *content, size_t size)
{
	free(content);
	content = NULL;
	(void) size;
}

char	*test_lstiter()
{
	t_list	*lst;
	t_list	*new_node;

	lst = NULL;
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstadd(&lst, new_node);
	ft_lstiter(lst, add);
	mu_assert("Wrong iterations", test_iter == 5);
	ft_lstdel(&lst, del_str);
	return (0);	
}
