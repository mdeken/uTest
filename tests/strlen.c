/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlen.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 17:51:13 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 17:56:37 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strlen()
{
	mu_assert("ft_strlen(\"\") != 0", ft_strlen("") == 0);
	mu_assert("ft_strlen(\"a\") != 1", ft_strlen("a") == 1);
	mu_assert("ft_strlen(\"aa\") != 2", ft_strlen("aa") == 2);
	mu_assert("ft_strlen(\"abc\") != 3", ft_strlen("abc") == 3);
	mu_assert("ft_strlen(\"Lorem Ipsum\") != 11", ft_strlen("Lorem Ipsum") == 11);
	mu_assert("ft_strlen(\"Coucou\\0Bonjour\") != 6", ft_strlen("Coucou\0Bonjour") == 6);
	mu_assert("ft_strlen(\"\\0\\0\\0\\0\") != 0", ft_strlen("\0\0\0\0") == 0);
	return (0);
}
