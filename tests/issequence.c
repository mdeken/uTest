/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   issequence.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:13:18 by mdeken            #+#    #+#             */
/*   Updated: 2016/11/10 15:27:09 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_issequence()
{
	mu_assert("ft_issequence(\"\\b\") != '\\b')", ft_issequence("\\b") == 8);
	mu_assert("ft_issequence(\"\\c\") != '\\0')", ft_issequence("\\c") == '\0');
	mu_assert("ft_issequence(\"\\f\") != '\\f')", ft_issequence("\\f") == 12);
	mu_assert("ft_issequence(\"\\n\") != '\\n')", ft_issequence("\\n") == '\n');
	mu_assert("ft_issequence(\"\\r\") != '\\r')", ft_issequence("\\r") == 13);
	mu_assert("ft_issequence(\"\\t\") != '\\t')", ft_issequence("\\t") == '\t');
	mu_assert("ft_issequence(\"\\v\") != '\\v')", ft_issequence("\\v") == 11);
	mu_assert("ft_issequence(\"\\\\\") != '\\')", ft_issequence("\\\\") == '\\');
	return (0);
}
