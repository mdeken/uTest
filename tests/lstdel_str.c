#include "minunit.h"
#include <stdlib.h>

char	*test_lstdel_str()
{
	t_list	*lst;
	t_list	*new_node;

	lst = NULL;
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstadd(&lst, new_node);
	ft_lstdel(&lst, ft_lstdel_str);
	mu_assert("List not deleted (lst != NULL)", lst == NULL);
	return (0);
}
