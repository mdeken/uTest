/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isalpha.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 15:29:25 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 16:34:59 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_isalpha()
{
	mu_assert("ft_isalpha('a') == 0", ft_isalpha('a') > 0);
	mu_assert("ft_isalpha('z') == 0", ft_isalpha('z') > 0);
	mu_assert("ft_isalpha('\\n') != 0", ft_isalpha('\n') == 0);
	mu_assert("ft_isalpha('A') == 0", ft_isalpha('A') > 0);
	mu_assert("ft_isalpha('Z') == 0", ft_isalpha('Z') > 0);
	mu_assert("ft_isalpha('0') != 0", ft_isalpha('0') == 0);
	mu_assert("ft_isalpha(' ') != 0", ft_isalpha(' ') == 0);
	mu_assert("ft_isalpha('e') == 0", ft_isalpha('e') > 0);
	mu_assert("ft_isalpha('E') == 0", ft_isalpha('E') > 0);
	mu_assert("ft_isalpha('\\t') != 0", ft_isalpha('\t') == 0);
	mu_assert("ft_isalpha(89624) != 0", ft_isalpha(89624) == 0);
	mu_assert("ft_isalpha('+') != 0", ft_isalpha('+') == 0);
	mu_assert("ft_isalpha(5) != 0", ft_isalpha(5) == 0);
	mu_assert("ft_isalpha(-1) != 0", ft_isalpha(-1) == 0);
	return (0);
}
