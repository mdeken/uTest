#include "minunit.h"
#include <stdlib.h>

char	*test_strjoin()
{
	char	*test;

	mu_assert("ft_stroin(NULL, NULL) != NULL", ft_strjoin(NULL, NULL) == NULL);
	test = ft_strjoin("coucou", NULL);
	mu_assert("ft_strjoin(\"coucou\", NULL) != \"coucou\"", strcmp("coucou", test) == 0);
	free(test);
	test = ft_strjoin(NULL, "coucou");
	mu_assert("ft_strjoin(NULL, \"coucou\") != \"coucou\"", strcmp(test, "coucou") == 0);
	free(test);
	test = ft_strjoin("hello", "coucou");
	mu_assert("ft_strjoin(\"hello\", \"coucou\") != \"hellocoucou\"", strcmp(test, "hellocoucou") == 0);
	free(test);
	test = ft_strjoin("", "coucou");
	mu_assert("ft_strjoin(\"\", \"coucou\") != \"coucou\"", strcmp(test, "coucou") == 0);
	free(test);
	test = ft_strjoin("coucou", "");
	mu_assert("ft_strjoin(\"coucou\", \"\") != \"coucou\"", strcmp(test, "coucou") == 0);
	free(test);
	return (0);
}
