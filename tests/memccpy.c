#include "minunit.h"

char	*test_memccpy()
{
	char	str1[] = "Hello coucou.";
	char	str2[30];
	void	*test1, *test2;

	test1 = ft_memccpy(str2, str1, 'o', 8);
	test2 = memccpy(str2, str1, 'o', 8);
	mu_assert("Wrong returned adress", test1 == test2);
	test1 = ft_memccpy(str2, str1, 'w', 5);
	mu_assert("NULL is not returned when c is not found", test1 == NULL);
	test1 = ft_memccpy(str2, str1, 0, 0);
	test2 = memccpy(str2, str1, 0, 0);
	mu_assert("ft_memccpy(str2, str1, 0, 0) != memccpy(str2, str1, 0, 0)", test1 == test2);
	test1 = ft_memccpy(str2, str1, 0, 13);
	test2 = memccpy(str2, str1, 0, 13);
	mu_assert("ft_memccpy(str2, str1, 0, 13) != memccpy(str2, str1, 0, 13)", test1 == test2);
	return (0);
}
