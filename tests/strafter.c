/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strafter.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 13:52:37 by mdeken            #+#    #+#             */
/*   Updated: 2016/12/08 14:02:45 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"
#include <stdlib.h>
#include <stdio.h>

char	*test_strafter()
{
	char	*test;

	test = ft_strafter("hello", 'e');
	mu_assert("ft_strafter(\"hello\", 'e') != \"llo\"", strcmp(test, "llo") == 0);
	free(test);
	test = ft_strafter("hello", 'h');
	mu_assert("ft_strafter(\"hello\", 'h') != \"ello\"", strcmp(test, "ello") == 0);
	free(test);
	test = ft_strafter("hello", '\0');
	mu_assert("ft_strafter(\"hello\", '\\0') != NULL", test == NULL);
	test = ft_strafter("hello", 'w');
	mu_assert("ft_strafter(\"hello\", 'w') != NULL", test == NULL);
	return (0);
}
