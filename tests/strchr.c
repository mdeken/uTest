/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strchr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 19:11:15 by mdeken            #+#    #+#             */
/*   Updated: 2016/10/22 19:19:04 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minunit.h"

char	*test_strchr()
{
	char	test[] = "qwertyuiop";

	mu_assert("ft_strchr(test, 'q') != strchr(test, 'q')", ft_strchr(test, 'q') == strchr(test, 'q'));
	mu_assert("ft_strchr(test, 'p') != strchr(test, 'p')", ft_strchr(test, 'p') == strchr(test, 'p'));
	mu_assert("ft_strchr(test, 'f') != 0", ft_strchr(test, 'f') == 0);
	mu_assert("ft_strchr(test, 0) != strchr(test, 0)", ft_strchr(test, 0) == strchr(test, 0));
	return (0);
}
