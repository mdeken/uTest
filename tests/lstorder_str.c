#include "minunit.h"
#include <stdlib.h>

static void	free_node(void *content, size_t size)
{
	char	*a;

	(void) size;
	a = (char *)content;
	if (a != NULL)
		free(a);
	size = 0;
}

static int	test_order(t_list *lst, int asc)
{
	char	*a;
	char	*a1;
	char	*b;
	char	*b1;
	char	*c;
	char	*c1;
	char	*end;

	if (asc == -1)
	{
		a = (char *)lst->content;
		a1 = (char *)lst->next->content;
		b = (char *)lst->next->next->content;
		b1 = (char *)lst->next->next->next->content;
		c = (char *)lst->next->next->next->next->content;
		c1 = (char *)lst->next->next->next->next->next->content;
	}
	else if (asc == 1)
	{
		c1 = (char *)lst->content;
		c = (char *)lst->next->content;
		b1 = (char *)lst->next->next->content;
		b = (char *)lst->next->next->next->content;
		a1 = (char *)lst->next->next->next->next->content;
		a = (char *)lst->next->next->next->next->next->content;

	}
	end = (char *)lst->next->next->next->next->next->next;
	if (strcmp(a, "a") != 0 || strcmp(a1, "a") != 0 || strcmp(b, "b") != 0 || strcmp(b1, "b") != 0 || strcmp(c, "c") != 0 || strcmp(c1, "c") != 0 || end != NULL)
		return (0);
	return (1);
}

char		*test_lstorder_str()
{
	t_list	*lst;
	t_list	*new_node;

	lst = NULL;
	new_node = NULL;
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	mu_assert("ft_lstaddorderby(NULL, NULL) -> != NULL", lst == NULL && new_node == NULL);
	new_node = ft_lstnew("c", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	mu_assert("ft_lstaddorderby(&lst, new_node,...) -> lst != new_node", lst == new_node);
	mu_assert("List does not end by NULL", lst->next == NULL);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	new_node = ft_lstnew("c", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, 1);
	mu_assert("Wrong order descending", test_order(lst, 1));
	ft_lstdel(&lst, free_node);
	new_node = ft_lstnew("c", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, -1);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, -1);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, -1);
	new_node = ft_lstnew("c", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, -1);
	new_node = ft_lstnew("a", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, -1);
	new_node = ft_lstnew("b", sizeof(char *));
	ft_lstaddorderby(&lst, new_node, ft_lstorder_str, -1);
	mu_assert("Wrong order ascending", test_order(lst, -1));
	ft_lstdel(&lst, free_node);
	return (0);
}
