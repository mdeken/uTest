#include "minunit.h"
#include "colors.h"
#include <stdio.h>

int tests_run;

static	void	all_tests()
{
	mu_run_test(test_atoi);
	mu_run_test(test_bitslen);
	mu_run_test(test_bzero);
	mu_run_test(test_convert_ctrl);
	mu_run_test(test_errno);
	mu_run_test(test_fmin);
	mu_run_test(test_get_env);
	mu_run_test(test_imemset);
	mu_run_test(test_imin);
	mu_run_test(test_isalnum);
	mu_run_test(test_isalpha);
	mu_run_test(test_isascii);
	mu_run_test(test_isdigit);
	mu_run_test(test_isprint);
	mu_run_test(test_issequence);
	mu_run_test(test_itoa);
	mu_run_test(test_lstadd);
	mu_run_test(test_lstaddback);
	mu_run_test(test_lstaddorderby);
	mu_run_test(test_lstdel);
	mu_run_test(test_lstdelone);
	mu_run_test(test_lstdel_str);
	mu_run_test(test_lstiter);
	mu_run_test(test_lstlen);
	mu_run_test(test_lstnew);
	mu_run_test(test_lstorder_str);
	mu_run_test(test_memalloc);
	mu_run_test(test_memccpy);
	mu_run_test(test_memchr);
	mu_run_test(test_memcpy);
	mu_run_test(test_memcmp);
	mu_run_test(test_memdel);
	mu_run_test(test_memmove);
	mu_run_test(test_memset);
	mu_run_test(test_nbrlen);
	mu_run_test(test_pow);
	mu_run_test(test_putchar);
	mu_run_test(test_putchar_fd);
	mu_run_test(test_putendc);
	mu_run_test(test_putendc_fd);
	mu_run_test(test_putendl);
	mu_run_test(test_putendl_fd);
	mu_run_test(test_putnbr);
	mu_run_test(test_putnbr_fd);
	mu_run_test(test_putnbrl);
	mu_run_test(test_putnbrr);
	mu_run_test(test_putstr);
	mu_run_test(test_putstr_color);
	mu_run_test(test_putstr_fd);
	mu_run_test(test_putstrl);
	mu_run_test(test_putstrnr);
	mu_run_test(test_sqrt);
	mu_run_test(test_strafter);
	mu_run_test(test_strbefore);
	mu_run_test(test_strbegin);
	mu_run_test(test_strcat);
	mu_run_test(test_strcchr);
	mu_run_test(test_strcchr_last);
	mu_run_test(test_strchr);
	mu_run_test(test_strclr);
	mu_run_test(test_strcmp);
	mu_run_test(test_strcount);
	mu_run_test(test_strcpy);
	mu_run_test(test_strdel);
	mu_run_test(test_strdup);
	mu_run_test(test_strequ);
	mu_run_test(test_striter);
	mu_run_test(test_strjoin);
	mu_run_test(test_strjoinc);
	mu_run_test(test_strlen);
	mu_run_test(test_tolower);
	mu_run_test(test_toupper);
}

int main(int argc, char **argv) {
	(void)argc;
	(void)argv;
	all_tests();
	printf("Tests run: %d\n", tests_run);
	return (0);
}
