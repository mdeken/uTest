/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdeken <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/22 16:25:07 by mdeken            #+#    #+#             */
/*   Updated: 2016/12/09 14:23:22 by mdeken           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTS_H
# define TESTS_H

char	*test_atoi();
char	*test_bitslen();
char	*test_bzero();
char	*test_convert_ctrl();
char	*test_errno();
char	*test_fmin();
char	*test_get_env();
char	*test_imemset();
char	*test_imin();
char	*test_isalnum();
char	*test_isalpha();
char	*test_isascii();
char	*test_isdigit();
char	*test_isprint();
char	*test_issequence();
char	*test_itoa();
char	*test_lstadd();
char	*test_lstaddback();
char	*test_lstaddorderby();
char	*test_lstdel();
char	*test_lstdelone();
char	*test_lstdel_str();
char	*test_lstiter();
char	*test_lstlen();
char	*test_lstnew();
char	*test_lstorder_str();
char	*test_memalloc();
char	*test_memccpy();
char	*test_memchr();
char	*test_memcmp();
char	*test_memcpy();
char	*test_memdel();
char	*test_memmove();
char	*test_memset();
char	*test_nbrlen();
char	*test_pow();
char	*test_putchar();
char	*test_putchar_fd();
char	*test_putendc();
char	*test_putendc_fd();
char	*test_putendl();
char	*test_putendl_fd();
char	*test_putnbr();
char	*test_putnbr_fd();
char	*test_putnbrl();
char	*test_putnbrr();
char	*test_putstr();
char	*test_putstr_color();
char	*test_putstr_fd();
char	*test_putstrl();
char	*test_putstrnr();
char	*test_sqrt();
char	*test_strafter();
char	*test_strbefore();
char	*test_strbegin();
char	*test_strcat();
char	*test_strcchr();
char	*test_strcchr_last();
char	*test_strchr();
char	*test_strclr();
char	*test_strcmp();
char	*test_strcount();
char	*test_strcpy();
char	*test_strdel();
char	*test_strdup();
char	*test_strequ();
char	*test_striter();
char	*test_strjoin();
char	*test_strjoinc();
char	*test_strlen();
char	*test_tolower();
char	*test_toupper();

#endif
